package spotify.demo.configs;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotEmpty;


@Component 
@Data 
@JsonIgnoreProperties(ignoreUnknown = true) 
@PropertySource("classpath:application.properties")
public class SpotifyConnectionConfig {

    @Value("${spotify.client-id}")
    @NotEmpty
    private String clientId;

    @Value("${spotify.client-secret}")
    @NotEmpty
    private String clientSecret;

    @Value("${spotify.authorization.redirectURL}")
    @NotEmpty
    private String spotifyAuthorizationRedirectURL;

    private String authorizeUrl = "https://accounts.spotify.com/authorize/";

    private String userInfoUri = "https://api.spotify.com/v1/me";    
    
    private String authorizeResponseType = "code";  

    private String authorizationScope = "user-read-private playlist-read-private playlist-modify-public user-library-read user-read-recently-played streaming app-remote-control";

    private String tokenUrl = "https://accounts.spotify.com/api/token";

    private String playerUrlRecentlyPlayed = "https://api.spotify.com/v1/me/player/recently-played";

    private String albumUrl = "https://api.spotify.com/v1/albums/";
   
    private String getAllPlayListsUrl = "https://api.spotify.com/v1/playlists/playlist_id";
    
    
}
