package spotify.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import spotify.demo.services.SpotifyUrlService;

@RestController
@RequestMapping("/v2/")
public class SpotifyContoller {
	
	@Autowired
	private  SpotifyUrlService spotifyUrlService;
	
	@GetMapping(value = "/getToken", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String showIndex() {
		return	spotifyUrlService.getAuthorizationURL();
		 
	}

}
