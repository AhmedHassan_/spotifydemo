package spotify.demo.services;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import lombok.Data;
import lombok.var;
import spotify.demo.configs.SpotifyConnectionConfig;
import spotify.demo.services.utils.CodeChallengeUtility;
import spotify.demo.services.utils.CodeVerifierUtility;

@Data
@Service
@EnableConfigurationProperties
public class SpotifyUrlService {
	private  SpotifyConnectionConfig spotifyConnectionConfig;
	private String codeVerifier;

	public String getAuthorizationURL() {
		
		final var codeVerifier = CodeVerifierUtility.generate();
		setCodeVerifier(codeVerifier);
		return "https://accounts.spotify.com/en/authorize?client_id=" + spotifyConnectionConfig.getClientId()
				+ "&response_type=code&redirect_uri=" + spotifyConnectionConfig.getSpotifyAuthorizationRedirectURL()
				+ "&code_challenge_method=S256&code_challenge=" + CodeChallengeUtility.generate(codeVerifier)
				+ "&scope=ugc-image-upload,user-read-playback-state,user-modify-playback-state,user-read-currently-playing,streaming,app-remote-control,user-read-email,user-read-private"
				+ ",playlist-read-collaborative,playlist-modify-public,playlist-read-private,playlist-modify-private,user-library-modify,user-library-read,user-top-read,user-read-playback-position,user-read-recently-played,user-follow-read,user-follow-modify";
	}

}
