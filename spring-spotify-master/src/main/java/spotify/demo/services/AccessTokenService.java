package spotify.demo.services;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import spotify.demo.configs.SpotifyConnectionConfig;
import spotify.demo.entities.AccessTokenDto;

@Service
@EnableConfigurationProperties()
public class AccessTokenService {
	
	private  SpotifyUrlService spotifyUrlService;
	private  RestTemplate restTemplate;
	private  SpotifyConnectionConfig spotifyConnectionConfig;
	private static final String URL = "https://accounts.spotify.com/api/token";

	public String getToken(String code) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("client_id", spotifyConnectionConfig.getClientId());
		map.add("grant_type", "authorization_code");
		map.add("code", code);
		map.add("redirect_uri", spotifyConnectionConfig.getSpotifyAuthorizationRedirectURL());
		map.add("code_verifier", spotifyUrlService.getCodeVerifier());

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

		ResponseEntity<AccessTokenDto> response = restTemplate.postForEntity(URL, request, AccessTokenDto.class);
		return response.getBody().getAccess_token();
	}
	
	

}
