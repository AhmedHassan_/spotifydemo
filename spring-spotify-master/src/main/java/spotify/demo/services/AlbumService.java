package spotify.demo.services;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import spotify.demo.configs.SpotifyConnectionConfig;

import java.util.Arrays;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static spotify.demo.services.AuthorizationService.callAction;
import static spotify.demo.services.utils.RestCallUtils.checkResponseCodeExpected;

@Service
public class AlbumService {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    SpotifyConnectionConfig spotifyConnectionConfig;


    /**
     * Get album by id
     */
    public JSONObject getById(String token, String id) {
        
        String createIndexUrl = spotifyConnectionConfig.getAlbumUrl() + id;

        
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("Authorization", token);

       
        ResponseEntity<JSONObject> responseEntity = callAction(restTemplate, "getAlbum", createIndexUrl, GET,
                new HttpEntity<>(null, requestHeaders), JSONObject.class, null);

       
        checkResponseCodeExpected(responseEntity, Arrays.asList(NO_CONTENT, OK), "getAlbum");

        return responseEntity.getBody();
    }
    
}
